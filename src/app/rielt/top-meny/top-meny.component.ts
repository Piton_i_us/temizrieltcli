import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';

import { RieltService } from '../rielt.service';
import { topRieltFilters } from 'src/app/_models/topRieltFilters';

@Component({
  selector: 'app-top-meny',
  templateUrl: './top-meny.component.html',
  styleUrls: ['./top-meny.component.scss']
})
export class TopMenyComponent implements OnInit {
  public priceStartValue: string = "100000";
  public priceEndValue: string = "1500000";
  public typeOfAdvert:string[]= ["Satilir","Kiraya","Sutkalig  kiraya"];
  public typeOfObjectList:string[]= ["Ev","Bag","Garaj","Bina"];
  public extraInfo:string[]= ["Kupca","Kredit","Teze tikinti"];
 public textFormControl:boolean=false; 
 public filtersModel: FiltersModel = new FiltersModel();
 public IsDisabled:boolean = false;
 public cityList:string[]= ["Baki","Sumgayit","Gence"];
 public floorList:number[]= [];
 public metroList:string[]= ["28 May","Me`mar Acemi","Inshatcilar"];
 public comunnaList:string[]= ["Xatayi","Bileceri","8 km","Nasemi"];
 
 public  menyRieltFilters: topRieltFilters = new topRieltFilters();



  constructor(private rieltServices : RieltService) { }

  ngOnInit() {
    this.tempIniti();
    
  }
  // temp initialisation of virables  
  private tempIniti() {
    this.filtersModel.kredit=false;
    this.filtersModel.newBuilding=true;
    this.filtersModel.kupcha=false;

    for (let index = 1; index <= 50; index++) {
      this.floorList.push(index); 
    }
    
  }

  public filterAdverType(filterVal: any) {
  }
  public objCategory(filterVal: any) {
  }
  public updateCheckedOptions(item, event) {
    item.checked = !item.checked;
  }


  

private name() {
   this.rieltServices.getProducts()
  .subscribe((res: topRieltFilters) => {
    this.menyRieltFilters = res;
    console.log(this.menyRieltFilters);
  }, err => {
    console.log(err);
  });
}  



}

export class FiltersModel {
  public priceMinValue: string;
  public priceMaxValue: string;
  public typeOfAdvert:string;
  public typeOfObject:string;
  public squareMinValue:string;
  public squareMaxValue:string;
  public city:string;
  public kupcha:boolean;
  public kredit:boolean;
  public newBuilding:boolean;
  public metro:string;
  public floorMin:string;
  public floorMax:string;
  public comunna:string;
  
  constructor(){}

}
