import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() public sidenavToggle = new EventEmitter();
  // @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
private firstLanguage: string = "RU"; 
private secondLanguage: string = "AZ";   
  public curentLanguage: string;
  public additionalLanguage: string;
  constructor() { }
 
  ngOnInit() {

    this.curentLanguage=this.firstLanguage;
this.additionalLanguage= this.secondLanguage;
  }
 
  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }
}
