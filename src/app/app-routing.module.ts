import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './_helpers';
import { Role } from './_models';
import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './login/login.component';
import { RieltComponent } from './rielt/rielt.component';
import { HeaderComponent } from './header/header.component';
const routerOptions: ExtraOptions = {
  
    // Tell the router to use the hash instead of HTML5 pushstate.
    useHash: false,

    // In order to get anchor / fragment scrolling to work at all, we need to
    // enable it on the router.
    // Example https://www.geekstrick.com/fragment-url-in-angular-8/
    anchorScrolling: "enabled",



    // Once the above is enabled, the fragment link will only work on the
    // first click. This is because, by default, the Router ignores requests
    // to navigate to the SAME URL that is currently rendered. Unfortunately,
    // the fragment scrolling is powered by Navigation Events. As such, we
    // have to tell the Router to re-trigger the Navigation Events even if we
    // are navigating to the same URL.
    onSameUrlNavigation: "reload",

    // Let's enable tracing so that we can see the aforementioned Navigation
    // Events when the fragment is clicked.
    enableTracing: true,
    scrollPositionRestoration: "enabled",
    scrollOffset: [0, 64] ,  
};

const routes: Routes = [
  
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
},
 
{
  path: 'rielt',
  component:RieltComponent,
 // canActivate: [AuthGuard]
},
{
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Admin] }
},
{
    path: 'login',
    component: LoginComponent
},
{
  path: 'header',
  component: HeaderComponent
},
 // otherwise redirect to home
 { path: '**', redirectTo: '' }

// otherwise redirect to 404
//{path: '**', redirectTo: RoutesConfig.routes.error404}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,routerOptions )],
  exports: [RouterModule]
})
export class AppRoutingModule { }

