﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


import { PingPong } from '../_models/pingPong';
import { environment } from 'src/environments/environment';
import { User } from '../_models';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`${environment.apiUrl}/users`);
    }

    getById(id: string) {
        return this.http.get<User>(`${environment.apiUrl}/users/${id}`);
    }
    // controller with [AllowAnonymous]
    getPing() {
        return this.http.get<PingPong>(`${environment.apiUrl}/values/`);
    }
      // controller only for  [Authorize]
    getPingAuth() {
        return this.http.get<PingPong>(`${environment.apiUrl}/valuesadmin/`);
    }
}