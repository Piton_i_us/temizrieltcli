import { NgModule } from '@angular/core';

import {
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatInputModule,
    MatBadgeModule,
    MatGridListModule,
    MatSelectModule,
    MatFormFieldModule,
    MatChipsModule,
    MatTooltipModule,
    MatTableModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatRadioModule,
    MatCheckboxModule,
    MatSlideToggleModule
} from '@angular/material';

@NgModule({
    imports: [
        MatButtonModule,
        MatMenuModule,
        MatToolbarModule,
        MatIconModule,
        MatCardModule,
        MatSidenavModule,
        MatProgressSpinnerModule,
        MatListModule,
        MatInputModule,
        MatButtonModule,
        MatBadgeModule,
        MatGridListModule,
        MatFormFieldModule,
        MatSelectModule,
        MatRadioModule,        
        MatChipsModule,
        MatTooltipModule,
        MatTableModule,
        MatPaginatorModule,
        MatCheckboxModule,
        MatSlideToggleModule 
    ],
    exports: [
        MatButtonModule,
        MatMenuModule,
        MatToolbarModule,
        MatIconModule,
        MatCardModule,
        MatSidenavModule,
        MatProgressSpinnerModule,
        MatListModule,
        MatInputModule,
        MatBadgeModule,
        MatGridListModule,
        MatFormFieldModule,
        MatSelectModule,
        MatRadioModule,
        MatCheckboxModule, 
        MatChipsModule,
        MatTooltipModule,
        MatTableModule,
        MatPaginatorModule,
        MatSlideToggleModule
    ],
    providers: [
        MatDatepickerModule,
     ]
})
export class MaterialModule { }